
app.data = {
    init: function() {


    },
    saveConfig: function()
    {
        // This is the json where all the info will be stored
        var draggablesPositions = {};
        // Get all the draggable elements (elements with class draggable)
        var draggables = $(".draggable");

        // Iterate over all these draggable elements, storing their info
        for (var i = 0, l = draggables.length; i < l; i++) {
            var draggable = $(draggables[i]);
            draggablesPositions[draggable.attr("id")] = {
                id: draggable.attr("id"),
                css: {
                    left: draggable.css("left"),
                    top: draggable.css("top"),
                    width: draggable.css("width")
                }
            };
        }

        // Save it to the localStoreage. 
        localStorage.setItem("config", JSON.stringify(draggablesPositions));
        

    },
    loadConfig: function()
    {
        // Get the config from localStorage
        var config = JSON.parse(localStorage.getItem("config"));

        for (var i in config) {
            var draggable = config[i];

            for(var prop in draggable.css){
                $("#" + draggable.id).css(prop, draggable.css[prop]);
            }
            // Ensure position is absolute
            $("#" + draggable.id).css('position', 'absolute');
        }

    }
    
};