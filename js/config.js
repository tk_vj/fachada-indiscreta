var config = {
    // The directory where the videos are place. Do not forget the SLASH at the end. e.g. 'videos/'
    videosDirectory: 'video/',
    // The prefix for the big (background) videos. e.g. if a normal video is 0101.mov and
    // the prefiex is big then big video will be big0101.mov
    bigVideosPrefix: 'big',
    // Just the Mask videos, not the BIG (background) videos
    videosList: ['0101.mov', '0202.mov', '0303.mov', '0404.mov', '0505.mov',
        '0606.mov', '0707.mov', '0808.mov', '0909.mov', '1010.mov', '1111.mov', '1212.mov', '1302.mov'],
    
    // Volume of a video when playing in background
    backgroundPlayerVolume: 0.2, // From 0 to 1
            // Time after which a resolved pair is muted
    timeBeforeMute: 7000, // Miliseconds
    // The time that the second video will be playing before checking if it is resolved or not
    timeToResolve: 7000, // Miliseconds
    
}