app.player = {
    // indicates the id of the first member of the pair selected. It is false when there is not first member selected. 
    fisrtMemberSelectedId: false,
    // The number of videos
    videosNumber: 6,
    // The directory where the videos are place. Do not forget the SLASH at the end. e.g. 'videos/'
    videosDirectory: 'low/',
    // The prefix for the big (background) videos. e.g. if a normal video is 0101.mov and
    // the prefiex is big then big video will be big0101.mov
    bigVideosPrefix: 'big',
    // Just the Mask videos, not the BIG (background) videos
    videosList: ['0101.mov', '0201.mov', '0102.mov', '0202.mov', '0103.mov', '0203.mov', '0104.mov',
        '0204.mov', '0105.mov', '0205.mov', '0106.mov', '0206.mov', '0107.mov', '0207.mov',
        '0108.mov', '0208.mov', '0109.mov', '0209.mov', '0110.mov', '0210.mov', '0111.mov',
        '0211.mov', '0112.mov', '0212.mov'],
    // videos data estructure. It contains info of each video: firstMemberPos, secondeMemberPos and videoId
    videos: {},
    // indicates if a position (mask) is empty or not
    emptyPositions: {},
    // indicates if a position is resolved or not
    resolvedPositions: {},
    // keysLocked is true when a pair is being resolved, so the use can not select any other mask
    keysLocked: false,
    loopsBeforeMute: 2,
    timeBeforeMute: 10000, // Miliseconds
    // Init
    init: function() {

        this.emptyPositions = {
            '01': true,
            '02': true,
            '03': true,
            '04': true,
            '05': true,
            '06': true,
            '07': true,
            '08': true,
            '09': true,
            '10': true,
            '11': true,
            '12': true

        }

        // Hide all masks
        $('.mask').addClass('hidden');

        this.allocateVideos();
        this.placeVideos();
    },
    // Build up a video data estructure, where every videos is allocate in 2 positions (masks): firstMember and secondMember. 
    // The firstMember of every video is allocated in a correct position. The secondMember is allocated randomly among the empty positions
    allocateVideos: function() {
        var count = 0, max = 6;
        // Get the videosList array and shuffle it
        var videosListCopy = this.videosList.slice(0);

        var aShuffleVideosIds = $.shuffle(videosListCopy);

        // Iterate over the elements in the shuffled array (random) and get the first 6 VALID videos to 
        // place them in the "right position". A valid video is a video that fits in a position that is empty

        for (var i = 1, l = aShuffleVideosIds.length; i < l; i++) {
            var videoName = aShuffleVideosIds[i];

            // Get the right position of this video
            var parts = videoName.split(".");
            var id = parts[0];

            var rightPos = id.substring(2);

            // If it is empty, assing the video to this position
            if (this.emptyPositions[rightPos]) {

                this.videos[id] = {
                    id: id,
                    name: videoName,
                    firstMemberPos: rightPos
                };
                this.emptyPositions[rightPos] = false;
                count++;
                if (count === max) {
                    break;
                }
            }
        }

        // Here we have the 6 SELECTED videos with the firstMember position assigned
        // Now, we have to assign the secondMemeber positions for those 6 videos
        // Iterate over the videos assinging the secondMember position to the first available empty position

        for (var i in this.videos) {
            for (var j in this.emptyPositions) {
                if (this.emptyPositions[j]) {
                    this.videos[i]['secondMemberPos'] = j;
                    this.emptyPositions[j] = false;
                    break;
                }
            }
        }

        con("al final del for, tengo videos ", this.videos)

    },
    // Using the info in the video data estructure, place each video in the corresponding positions (masks)
    placeVideos: function() {

        for (var i in this.videos) {
            var video = this.videos[i];
            // Get the mask of the firstMember
            var maskFirstMember = $("#" + video.firstMemberPos);
            maskFirstMember.attr('src', this.videosDirectory + video.name);

            // Get the mask of the secondMember
            var maskSecondMember = $("#" + video.secondMemberPos);
            maskSecondMember.attr('src', this.videosDirectory + video.name);
        }

    },
    /// show a mask when it is selected
    showElement: function(id) {
        var element = $('#' + id);
        element.removeClass('hidden');
        element.get(0).play();
        $('#' + id).prop('muted', false);
    },
    // Hide a mask when it was selected but the pair did not match
    hideElement: function(id) {
        var element = $('#' + id);
        element.addClass('hidden');
        element.get(0).pause();
    },
    // An element was selected by the user
    selectElement: function(id) {

        // Check if keysLocked is true, this element is already shown or the element is resolved
        if (this.keysLocked || id === this.fisrtMemberSelectedId || this.resolvedPositions[id]) {
            return;
        }

        this.showElement(id);
        // First member is selected
        if (!this.fisrtMemberSelectedId) {
            // Mark as selected
            this.fisrtMemberSelectedId = id;
        }
        // SecondMember
        else {
            this.keysLocked = true;
            // Wait for some time (2 seconds) to check if the pair matches
            window.setTimeout(function() {
                // Check if is coupled with the first member
                var isMatched = app.player.isMatched(id, app.player.fisrtMemberSelectedId);

                if (!isMatched) {
                    app.player.hideElement(id);
                    app.player.hideElement(app.player.fisrtMemberSelectedId);
                }
                else {
                    app.player.markAsResolved(id, app.player.fisrtMemberSelectedId);
                }
                app.player.fisrtMemberSelectedId = false;
                app.player.keysLocked = false;

            }, 2000);
        }
    },
    // Return if 2 positions are matched (same video)    
    isMatched: function(firstElementId, secondElementId) {

        for (var i in this.videos) {
            var video = this.videos[i];
            if ((video.firstMemberPos == firstElementId && video.secondMemberPos == secondElementId)
                    || (video.firstMemberPos == secondElementId && video.secondMemberPos == firstElementId)) {

                return true;
            }
        }

        return false;
    },
    // get a position id and check if this position is the correctPos of the pair
    isCorrectPos: function(id) {
        for (var i in this.videos) {
            var video = this.videos[i];
            if (video.firstMemberPos == id) {
                return true;
            }
        }

        return false;
    },
    // Mark a pair as resolved
    markAsResolved: function(firstElementId, secondElementId) {

        this.resolvedPositions[firstElementId] = this.resolvedPositions[secondElementId] = true;



        var correctId;

        if (this.isCorrectPos(firstElementId)) {
            correctId = firstElementId;
        }
        else {
            correctId = secondElementId;
        }

        // Get the first number of the elementId: 1 or 2
        var andarNumber = Number(correctId) > 6 ? 0 : 1;
        // Get the background mask 
        var backgroundMask = $('#andar' + andarNumber);
        var maskSrc = $('#' + correctId).attr('src');
        // This is kind of strange but effective. We change the directory for the directory + prefix.
        // e.g. in low/0101.mov we change low/ for low/big so we obtain low/big0101.mov
        var backgroundSrc = maskSrc.replace(this.videosDirectory, this.videosDirectory + this.bigVideosPrefix);
        backgroundMask.attr('src', backgroundSrc);

        // Mark the background as resolved
        this.resolvedPositions[backgroundMask.attr('id')] = true;

        backgroundMask.removeClass('hidden');
        backgroundMask.get(0).play();

        // The masks (windows / doors) will be kept visible and playing but they will be
        // muted after some seconds
        setTimeout(function() {
            con("pongo el video en mute");
            $('#' + firstElementId).prop('muted', true);
            $('#' + secondElementId).prop('muted', true);
        }, this.timeBeforeMute)


    },
    showResolvedMasks: function() {
        $('.mask').addClass('hidden');

        for (var i in this.resolvedPositions) {

            if (this.resolvedPositions[i]) {
                con("mostrar!")
                $('#' + i).removeClass('hidden');
            }
        }

    }


}