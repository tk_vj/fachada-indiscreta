app.edition = {
    init: function() {


        // Load the configuration and change the position of the elements
        app.data.loadConfig();

        this.setMasksAsDraggable();



    },
    setMasksAsDraggable: function() {
        // Iterate over all the elements with the CLASS draggable, make them draggables
        $(".draggable").each(function(index, element) {

            $(element).draggable();

            // On mouse down over an element, make all the other elements semi-transparent
            // and the element opaque
            $(element).mousedown(function() {

                app.selectedMaskId = $(element).attr("id");

                $(".draggable").css("opacity", 0.6);
                $(element).css("opacity", 1);
            });
        });
    },
    
    setMasksAsNotDraggable: function() {
        // Iterate over all the elements with the CLASS draggable, make them draggables
        $(".draggable").each(function(index, element) {

            $(element).draggable('destroy');

            $(element).unbind('mousedown');
        });
    },
    
    showAllMasks: function(){
        
            $('.mask').removeClass('hidden');
            
    },
            
    unselectMask: function() {
        $(".draggable").css("opacity", 1);
        app.selectedMaskId = null;
    }
};