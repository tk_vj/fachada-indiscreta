

var app = {
    selectedMaskId: null,
    // Current mode: edition or player
    currentMode: null,
    init: function() {
        
        app.edition.init();
        app.player.init();
        // It always start as player
        this.setMode('player');

    },
    // Set mode Edition or Player
    setMode: function(mode) {
        this.currentMode = mode;
        if (mode === 'edition') {
            $('#editionModeIndicator').removeClass('hidden');
            app.edition.setMasksAsDraggable();
            
            // Show all elements
            app.edition.showAllMasks();
        }
        else {
            $('#editionModeIndicator').addClass('hidden');
            app.edition.setMasksAsNotDraggable();
            $('.mask').css('opacity', 1);
            app.player.showResolvedMasks();
            
        }
    }

};



$(document).ready(function() {

    // Add a shuffle function
    $.shuffle = function(arr) {

        for (var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x)
            ;
        return arr;
    };

    app.init();

});

///// Funciones para el desarrollo 
function con() {
    try {
        for (var i = 0, l = arguments.length; i < l; i++) {
            console.log(arguments[i]);
        }
    }
    catch (e) {

    }
}
///////
