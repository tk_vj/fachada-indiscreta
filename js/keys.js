document.onkeydown = function(e) {
    con(e.which)
// Cursors to move mask, just avaiblable when mask is selected
    if (e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
        if (!app.selectedMaskId || app.currentMode !== 'edition')
            return;
    }

    // Get the selected mask
    var selectedMask = $("#" + app.selectedMaskId);
    switch (e.which) {

        // EDIT. Cursors
        // up
        case 38:
            if (e.shiftKey) {
                selectedMask.animate({width: '+=1'}, 0);
            }
            else {
                selectedMask.animate({top: '-=1'}, 0);
            }
            e.preventDefault();
            break;

            // down
        case 40:
            if (e.shiftKey) {
                selectedMask.animate({width: '-=1'}, 0);

            }
            else {
                selectedMask.animate({top: '+=1'}, 0);

            }
            e.preventDefault();
            break;

            // left
        case 37:
            if (e.shiftKey) {

                selectedMask.animate({height: '-=1'}, 0);
            }
            else {
                selectedMask.animate({left: '-=1'}, 0);
            }

            e.preventDefault();
            break;



            // right
        case 39:
            if (e.shiftKey) {
                selectedMask.animate({height: '+=1'}, 0);
            }
            else {
                selectedMask.animate({left: '+=1'}, 0);
            }

            e.preventDefault();
            break;
            // U (unselect)
        case 85:
            app.edition.unselectMask();
            e.preventDefault();
            break;


// CHANGE MODE
        case 77:
            var mode = app.currentMode === 'edition' ? 'player' : 'edition';
            app.setMode(mode);
            e.preventDefault();
            break;

            // PLAYER
            // UP
            // 1
        case 49:
            app.player.selectElement('01');
            e.preventDefault();
            break;
            // 2
        case 50:
            app.player.selectElement('02');
            e.preventDefault();
            break;

            // 3
        case 51:
            app.player.selectElement('03');
            e.preventDefault();
            break;

            // 4
        case 52:
            app.player.selectElement('04');
            e.preventDefault();
            break;

            // 5
        case 53:
            app.player.selectElement('05');
            e.preventDefault();
            break;

            // 6
        case 54:
            app.player.selectElement('06');
            e.preventDefault();
            break;



            // DOWN
            // q
        case 81:
            app.player.selectElement('07');
            e.preventDefault();
            break;
            // w
        case 87:
            app.player.selectElement('08');
            e.preventDefault();
            break;

            // e
        case 69:
            app.player.selectElement('09');
            e.preventDefault();
            break;

            // r
        case 82:
            app.player.selectElement('10');
            e.preventDefault();
            break;

            // t
        case 84:
            app.player.selectElement('11');
            e.preventDefault();
            break;

            // y
        case 89:
            app.player.selectElement('12');
            e.preventDefault();
            break;



            // Save Config   S
        case 83:
            app.data.saveConfig()
            e.preventDefault();
            break;

            // Load Config   L
        case 76:
            app.data.loadConfig()
            e.preventDefault();
            break;




    }


};
